# Programming Competition for High Schools -- Round 2

## Submission instructions

To submit your solutions on the Gitlab account of the competition, first, you need to name your submission following this format: <team-name>-round-2.ipynb. For example, assuming you are team1 your submission should be named team1-round-2.ipynb. If you have multiple submissions for the current round, you should add a counter to the submission name as follows: team1_1-round-2.ipynb, team1_2-round-2.ipynb, etc.

All submissions should be uploaded in the round2 subfolder under submissions. Please note that the submissions folder is accessible to all participants. As such, you should refrain from uploading your file before __17:00 PM__, the end of the second round.

## Problem 1

Write a program that accepts a sentence (a collection of words) as input displays the word (or words) that appears the most.

Example:
* Please enter the sentence: when I leave home I miss home 
* Output: "home" appears two times, "I" appears two times.

## Problem 2

Due to the outbreak of Covid-19 pandemic, Namibia is as well trying to find solutions to to manage further spreading of the disease effectively. Your programming competition team has been chosen to use computing technology to create awareness by informing the public about the symptoms.  Think about an array of most common symptoms and an array of less common ones. Once a symptom is entered, the system should be able to compare if the user/patient has the less common or most common symptoms and couple that with the temperature range to determine the likelihood of the user/patient to have contracted COVID-19.

We consider the following symptoms:
1. Most common symptoms
	* fever
	* dry cough
	* tiredness
2. Less common symptoms
	* aches and pains
	* sore throat
	* diarrhoea 
	* conjunctivitis 
	* headache 
	* loss of taste or smell 
	* a rash on the skin, or discolouration of fingers or toes

Write a program that displays the symptoms per category. Extend the program to detect whether a patient is suspected of having contracted COVID-19. The program should take as input the body temperature, any visit of a high-risk area in the last fourteen days and the symptoms that the user has experienced lately. The conjunction of (high) fever (temperature higher than 37), contact with a high-risk area and a display of common symptoms means a strong likelihood of the user to have COVID-19.

Example:
* Case 1
	* Enter your temperature: 36.7
	* Have you visited a high-risk area in the last fourteen days?: No
	* Did you experience any COVID-19 symptoms?: No
	* The likelihood that you might have contracted COVID-19 is rather low
* Case 2
	* Enter your temperature: 37.8
	* Have you visited a high-risk area in the last fourteen days?: Yes
	* Did you experience any COVID-19 symptoms?: dry cough, tiredness
	* The likelihood that you might have contracted COVID-19 is quite strong

## Problem 3

Given an alphanumeric phone number, convert it to all digits. For example, given 081-FLOWERS, your application will convert it to 081-3569377. Notice the hyphens maintain their location to match their position in the original phone number. An image of a phone keypad is provided below. All letters in the phone number will be uppercase only. Run your application once for each set of test data.

![screen capture](../images/keypad.png)

The program will accept as input a valid alphanumeric phone number consisting of the number 081 followed by a dash followed by any combination of the digits 1 through 9, dashes, and uppercase letters in the alphabet. Read the phone number from the user input. As output, it returns the equivalent phone number converted to all numbers. Display the output on the monitor. See the sample output below.

Examples:
* Example 1
	* Input: Please enter the phone number: 081-FLOWERS
	* Output: 081-3569377
* Example 2
	* Input: Please enter the phone number: 081-GOFEDEX
	* Output: 081-4633339
* Example 3
	* Input: Please enter the phone number: 081-FOR-RENT
	* Output: 081-367-7368
