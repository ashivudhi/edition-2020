# [Python](https://www.python.org/downloads/) Notebooks

A Python *notebook* application allows to write and execute Python programs in a Web browser (e.g., Internet Explorer, Firefox, Google Chrome, Safari). You can also enrich your notebook with comments and documentation. In this short tutorial, we will discuss how to install a Jupyter notebook application, start it and write and execute Python programs.

## Installation

Throughout this tutorial, we will assume that you have Python installed on your local machine. In case you do not, please go to the [Python](https://www.python.org/downloads/) website, follow the instructions and install it for your operating system (e.g., Windows, Mac, linux). By default, we will assume that you have Python 3 installed on your machine. If you are not sure which Python distribution to use or new to Python, we recommend [Anaconda](https://www.anaconda.com/products/individual).

To install Jupyter notebook, you can use the Python package manager *pip*. Here, we will use *pip3*. First, make sure to upgrade your pip installation by executing the command `pip3 install --upgrade pip` in a terminal. To install Jupyter, run the command `pip3 install jupyter` in a terminal.

## Run a Notebook

To use a Jupyter notebook, simply issue the command `jupyter notebook`. When the command runs successfully, it display some information on the terminal, including how to access the notebook app. By default, the notebook application is accessible at http://localhost:8888. So open your favourite web browser and visit the link. You will have access to the notebook dashboard where all notebooks are listed. If you want to open a specific notebook, for example example1.ipynb, please issue the command `jupyter notebook example1.ipynb`. You can also create a new notebook by following the menu on the interface in the browser.

![screen capture](../images/notebook-sc.png)
The screen capture above shows a *cell* in a notebook and the menu for edition. To add a new cell in a notebook, click on the + button.
